output "ssh_key_name" {
  value = "${aws_key_pair.ssh_key.key_name}"
}

output "ssh_key_fingerprint" {
  value = "${aws_key_pair.ssh_key.fingerprint}"
}
