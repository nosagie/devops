variable "access_key" {}
variable "secret_key" {}

variable "region" {
  default = "eu-west-1"
}

variable "dev_sshpubkey_file" {}
variable "prod_sshpubkey_file" {}
